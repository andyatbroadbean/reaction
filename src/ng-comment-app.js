angular.module('commentBoxFilters', []).filter('markdown', function($sce) {
  var converter = new Showdown.converter();
  return function(input) {
    return $sce.trustAsHtml(converter.makeHtml(input));
  };
});

var commentBoxApp = angular.module('commentBoxApp', ['commentBoxFilters']);
commentBoxApp.controller('CommentBoxCtrl', function ($scope, $http) {
  $http.get('comments.json').success(function(data) {
    $scope.comments = data;
  });
});
