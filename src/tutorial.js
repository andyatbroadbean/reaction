/** @jsx React.DOM */

"use strict";

var CommentList = React.createClass({
  render: function() {
    // can do this.props.author to access the author
    // and this.props.children to access the inner html?
    // this.props.children is React's wrapped text - call .toString()
    // to get the raw string
    var commentNodes = this.props.data.map(function (comment) {
      return <Comment author={comment.author}>{comment.text}</Comment>;
    });

    return (
      <div className="commentList">
        {commentNodes}
      </div>
    );
  }
});

// className gets transformed to class. Why is it called className?
// ~~need to think in terms of javascript named attributes NOT html~~ NOT TRUE!
// identifiers such as `class` and `for` are discouraged because JSX is javascript
// so use className and htmlFor instead
// see http://facebook.github.io/react/docs/dom-differences.html
var CommentForm = React.createClass({
  handleSubmit: function(event) {
    event.preventDefault();

    var author = this.refs.author.getDOMNode().value.trim();
    var text   = this.refs.text.getDOMNode().value.trim();
    if (!text || !author) {
      return false;
    }

    this.props.onCommentSubmit({author: author, text: text});

    this.refs.author.getDOMNode().value = '';
    this.refs.text.getDOMNode().value = '';
  },
  render: function() {
    return (
      <fieldset>
        <legend>Add a comment</legend>
        <form className="commentForm" onSubmit={this.handleSubmit}>
          <input type="text" placeholder="Your name (required)" ref="author" required />
          <input type="text" placeholder="Say something... (required)" ref="text" required />
          <input type="submit" className="button radius" value="Post" />
        </form>
      </fieldset>
    );
  }
});

var CommentBox = React.createClass({
  calcLastUpdated: function() {
    if ( this.state.lastUpdate ) {
        this.setState({elapsed: Date.now() - this.state.lastUpdate});
    }
  },
  lastUpdatedAgo: function() {
    if ( typeof(this.state.elapsed) === 'undefined' ) {
        return 'Not updated yet';
    }

    return 'Fetched ' + this.state.elapsed + 'ms ago';
  },
  loadCommentsFromServer: function() {
    console.debug("loadCommentsFromServer");
    $.ajax({
       url: this.props.url,
       dataType: 'json',
       success: function(data) {
         this.setState({data: data, lastUpdate: Date.now(), elapsed: 0});
       }.bind(this),
       error: function(xhr, status, err) {
         console.error(this.url, status, err.toString());
       }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    var comments = this.state.data;
    var newComments = comments.concat([comment]);
    this.setState({data: newComments});
    // TODO: send the new comment to the server
  },
  getInitialState: function() {
    console.debug("getInitialState");
    return {data: []};
  },
  componentWillMount: function() {
    console.debug("componentWillMount");
    setInterval(this.calcLastUpdated, 99);
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },

  render: function() {
    console.debug("render");
    return (
      <div className="commentBox">
        <h1>Mellow, world! I am a CommentBox. <small><small>{this.lastUpdatedAgo()}</small></small></h1>
        <CommentList data={this.state.data}/>
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
    );
  }
});

var converter = new Showdown.converter();
var Comment = React.createClass({
  render: function() {
    var rawMarkup = converter.makeHtml(this.props.children.toString());
    return (
      <div className="comment">
        <h3 className="commentAuthor">
          {this.props.author}
        </h3>
        <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
      </div>
    );
  }
});
